""" Example of solution of Frozen-Lake problem with Monte-Carlo methods.
You can set some of hyperparameters (including version 'FrozenLake-v1' / 'FrozenLake8x8-v1')
in common_components.py
"""

import random

import numpy as np
from tqdm import tqdm
from common_components import (env, GAMMA, n_states, n_actions, get_random_policy,
                               v2best_policy, q2best_policy, policy_score)


EPOCHS = 10000  # 100000

NONGREEDY_EPOCHS = 10

EPSILON = 0.5
EPSILON_DISCOUNT = 0.8  # epsilon epoch discounting coefficient


def run_episode(policy: np.array, epsilon: float = None):
    """Runs episode. Records and returns all states and rewards."""
    states = []
    actions = []
    rewards = []
    state = env.reset()
    is_done = False
    while not is_done:
        states.append(state)
        if epsilon:
            action = np.random.choice([policy[state].item(), random.choice(range(n_actions))],
                                      p=(1 - epsilon, epsilon))
        else:
            action = policy[state].item()
        actions.append(action)

        new_state, reward, is_done, info = env.step(action)
        rewards.append(reward)
        state = new_state
    return states, actions, rewards


def mc_all(policy: np.array = None, iterations: int = EPOCHS, epsilon: float = None):
    """ Estimates V (states values) with greedy All Visits Monte-Carlo algorithm
    with certain policy (default - random for each iteration).
    """
    V = np.zeros(n_states)  # values of each state (initialized with zeros)
    Q = np.zeros((n_states, n_actions))  # values of each action on each state (initialized with zeros)
    Nv = np.zeros(n_states)  # number of visits of each state
    Nq = np.zeros((n_states, n_actions))  # number of uses of each action

    for _ in tqdm(range(iterations), ncols=100):
        if policy is None:
            policy_ = get_random_policy()
        else:
            policy_ = policy

        return_t = 0
        states, actions, rewards = run_episode(policy_, epsilon)
        # all steps (states, actions and rewards) from end to start
        for state, action, reward in zip(reversed(states), reversed(actions), reversed(rewards)):
            return_t = reward + GAMMA * return_t

            V[state] += return_t
            Q[state, action] += return_t
            Nv[state] += 1
            Nq[state, action] += 1

    for state in range(n_states):
        if Nv[state]:
            V[state] = V[state] / Nv[state]
        for action in range(n_actions):
            if Nq[state, action]:
                Q[state, action] = Q[state, action] / Nq[state, action]

    return V, Q


def mc_first(policy: np.array = None, iterations: int = EPOCHS, epsilon: float = None):
    """ Estimates V (states values) with greedy First Visits Monte-Carlo algorithm
    with certain policy (default - random for each iteration).
    """
    V = np.zeros(n_states)  # values of each state (initialized with zeros)
    Q = np.zeros((n_states, n_actions))  # values of each action on each state (initialized with zeros)
    Nv = np.zeros(n_states)  # number of visits of each state
    Nq = np.zeros((n_states, n_actions))  # number of uses of each action

    for _ in tqdm(range(iterations), ncols=100):
        if policy is None:
            policy_ = get_random_policy()
        else:
            policy_ = policy

        Gv = np.zeros(n_states)  # local (for epoch) values of each state
        Gq = np.zeros((n_states, n_actions))  # local (for epoch) values of each action on each state
        first_vizit_v = np.zeros(n_states)
        first_vizit_q = np.zeros((n_states, n_actions))

        return_t = 0
        states, actions, rewards = run_episode(policy_, epsilon)
        # all steps (states, actions and rewards) from end to start
        for state, action, reward in zip(reversed(states), reversed(actions), reversed(rewards)):
            return_t = reward + GAMMA * return_t

            Gv[state] = return_t  # only first visit (last set, due to reversion) of state will affect
            Gq[state, action] = return_t  # only first usage (last set, due to reversion) of state-action will affect
            first_vizit_v[state] = 1
            first_vizit_q[state, action] = 1

        for state in range(n_states):
            if first_vizit_v[state]:
                V[state] += Gv[state]  # add to global V-values local (for epoch) Gv-values
                Nv[state] += 1  # increase by one global number of state-visits for final normalization
                for action in range(n_actions):
                    if first_vizit_q[state, action]:
                        Q[state, action] += Gq[state, action]  # add to global Q-values local (for epoch) Gq-values
                        Nq[state, action] += 1  # increase by one global number of state-visits for final normalization

    for state in range(n_states):
        if Nv[state] > 0:
            V[state] = V[state] / Nv[state]
            for action in range(n_actions):
                if Nv[state] > 0:
                    Q[state, action] = Q[state, action] / Nq[state, action]

    return V, Q


def mc_epsilon_greed(epochs=NONGREEDY_EPOCHS, epsilon: float = EPSILON):
    """Estimates best policy with epsilon-greedy First Visits Monte-Carlo algorithm."""
    P = get_random_policy()
    for epoch in range(epochs):
        print('\repoch =', epoch + 1, 'from', epochs)
        print('P ->', P, ', epsilon ->', epsilon)
        V, Q = mc_all(P, epsilon=epsilon)
        # V, Q = mc_first(P, epsilon=epsilon)
        epsilon *= EPSILON_DISCOUNT
        # P = v2best_policy(V)  # V works better
        P = q2best_policy(Q)  # but Q is more real (when transition law is unknown)

    return P


if __name__ == '__main__':
    V, Q = mc_all()
    # V, Q = mc_first()
    print('Values of states:', V)
    print('Values of actions:', Q)

    P = v2best_policy(V)
    print(' ... \U000023F3 ... WAIT ... \U000023F3 ... ', end='')
    print('\rBest policy', P, ', score =', policy_score(P, n=10000))

    # P = q2best_policy(Q)
    # print(' ... \U000023F3 ... WAIT ... \U000023F3 ... ', end='')
    # print('\rBest policy', P, ', score =', policy_score(P, n=10000))

    # P = mc_epsilon_greed()
    # print(' ... \U000023F3 ... WAIT ... \U000023F3 ... ', end='')
    # print('\rBest policy', P, ', score =', policy_score(P, n=10000))