""" Frozen Lake problem solution with value iteration.
You can set some of hyperparameters (including version 'FrozenLake-v1' / 'FrozenLake8x8-v1')
in common_components.py
"""

import numpy as np
from tqdm import tqdm

from common_components import env, n_states, n_actions, GAMMA, v2best_policy, policy_score


EPOCHS = 100000
THRESHOLD = None  # 1e-10  # Threshold of difference to stop iteration


state = env.reset()

V = np.zeros(n_states)  # initial state values
pbar = tqdm(range(EPOCHS), ncols=100)
for _ in pbar:
    q = np.zeros((n_states, n_actions))  # initial for epoch state-actions values
    for state in range(n_states):
        for action in range(n_actions):
            for prob, new_state, reward, is_done in env.env.P[state][action]:
                q[state, action] += prob * (reward + GAMMA * V[new_state])
            q[state, 0] = q[state].max()  # we store best action values in first column

    if THRESHOLD:
        diff = np.max(np.abs(V - q[:, 0]))
        pbar.set_postfix({'diff': diff})
        if diff < THRESHOLD:
            break

    V = q[:, 0]


print('Values of states:', V)

P = v2best_policy(V)
print('Best policy:', P, ', score =', policy_score(P, n=30000))
