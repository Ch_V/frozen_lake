""" Example of solution of Frozen-Lake problem with Genetic Algorithm.
You can set some of hyperparameters (including version 'FrozenLake-v1' / 'FrozenLake8x8-v1')
in common_components.py
"""

from functools import partial
import heapq
import multiprocessing as mp
import random
from typing import Tuple

import numpy as np
from tqdm import tqdm

from common_components import get_random_policy, policy_score, n_actions


LOAD_POPULATION = False  # 'filename' or False
SAVE_CURRENT_POPULATION = 'current_population.npy'  # 'filename' or False
USE_PARALLEL = True

EPOCHS = 15
POLICY_SCORE_N = 150  # iterations to run to estimate policy (identity)
FINAL_POLICY_SCORE_N = 1000  # iterations to run to estimate and choose final policy (identity)

POPULATION = 100
ELITE = 5
GROUP = 4
P_CROSSOVER = 0.5
P_MUTATION = 0.02


def select(popul: list, scores: list) -> list:
    """Tournament selection with groups by GROUP identities."""
    res_popul = []
    pop_scr = tuple(zip(popul, scores))
    while len(res_popul) < POPULATION:
        group = [i for i in random.choices(pop_scr, k=GROUP)]
        res_popul.append(max(group, key=lambda x: x[1])[0].copy())
    return res_popul


def cx_one_point(idn1: np.array, idn2: np.array, point: int = None) -> Tuple[np.array, np.array]:
    """One point cross-over."""
    if point is None:
        point = random.randint(0, len(idn1))
    idn1_ = idn1.copy()
    idn2_ = idn1.copy()
    idn1_[:point] = idn2[:point]
    idn2_[:point] = idn1[:point]
    return idn1_, idn2_


def crossover(popul: list, cx: callable = cx_one_point) -> list:
    """Applies cx crossover to popul population."""
    if ELITE:
        global elite  # list of elite identities
        res_popul = [i.copy() for i in elite]
    else:
        res_popul = []

    while len(res_popul) < POPULATION:
        # pop-add two random identities to group list crossover
        group = []
        group.append(popul.pop(random.randint(0, len(popul) - 1)))
        group.append(popul.pop(random.randint(0, len(popul) - 1)))
        if random.random() < P_CROSSOVER:
            # print('group_before_cx>', group)
            group = cx(*group)
            # print('group_after_cx>', group)

        res_popul.extend(group)

    return res_popul


def mutation(popul):
    """Random mutation."""
    for idn in popul:
        for i in range(len(idn)):
            if random.random() < P_MUTATION:
                idn[i] = random.randint(0, n_actions - 1)


# load population from file or generate random one
if LOAD_POPULATION:
    _np_population = np.load(LOAD_POPULATION)
    population = [i for i in _np_population]
else:
    population = [get_random_policy() for _ in range(POPULATION)]


if __name__ == '__main__':

    if USE_PARALLEL:
        pool = mp.Pool(processes=mp.cpu_count())

    pbar = tqdm(range(EPOCHS), ncols=100)
    for i in pbar:
        if USE_PARALLEL:
            scores = pool.map(partial(policy_score, n=POLICY_SCORE_N), population)
        else:
            scores = [policy_score(idn, n=POLICY_SCORE_N) for idn in population]

        elite = []
        elite.extend([i[0].copy() for i in heapq.nlargest(ELITE, tuple(zip(population, scores)), key=lambda x: x[1])])

        mean_score = sum(scores) / len(scores)
        max_score = max(scores)
        population = select(population, scores)
        population = crossover(population)
        mutation(population)

        population.extend(elite)

        # save current population;
        # you can put this out of evaluation cycle, but then you could not interrupt evaluation
        if SAVE_CURRENT_POPULATION:
            np.save(SAVE_CURRENT_POPULATION, population)

        pbar.set_postfix({'mean': mean_score, 'max': max_score})

    if USE_PARALLEL:
        pool.close()

    print(' ... \U000023F3 ... WAIT ... \U000023F3 ... ', end='')
    res = [(i, policy_score(i, n=FINAL_POLICY_SCORE_N)) for i in population]
    best = max(res, key=lambda x: x[1])
    print('\rBest policy:', best[0])
    print('Best policy score:', best[1])
