""" Frozen Lake problem solution with strategy (policy) iteration.
You can set some of hyperparameters (including version 'FrozenLake-v1' / 'FrozenLake8x8-v1')
in common_components.py
"""

import numpy as np
from tqdm import tqdm

from common_components import env, n_states, n_actions, GAMMA, get_random_policy, p2v, policy_score


EPOCHS = 100
THRESHOLD = 1e-10  # None  # Threshold of difference to stop iteration


state = env.reset()

P = get_random_policy()
pbar = tqdm(range(EPOCHS), ncols=100)
for _ in pbar:
    P_old = P.copy()
    V = p2v(P)

    for state in range(n_states):
        action_v = np.zeros(n_actions)  # actions values for current state
        for action in range(n_actions):
            for prob, new_state, reward, is_done in env.env.P[state][action]:
                action_v[action] += prob * (reward + GAMMA * V[new_state])
        P[state] = action_v.argmax()

    if THRESHOLD:  # stop when P == P_old if THRESHOLD is set
        diff = np.max(np.abs(P - P_old))
        pbar.set_postfix({'diff': str(diff)})
        if diff <= THRESHOLD:
            break


print('Values of states:', V)
print('Best policy:', P, ', score =', policy_score(P, n=30000))

