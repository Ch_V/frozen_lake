""" Here toy can play FrozenLake game manual by entering step-corresponding digits into input.
You can set some of hyperparameters (including version 'FrozenLake-v1' / 'FrozenLake8x8-v1')
in common_components.py
"""

import gym

from common_components import env

state = env.reset()

is_done=False
while not is_done:
    env.render()
    try:
        action = int(input("0 <-> 'left'\n"
                           "1 <-> 'down'\n"
                           "2 <-> 'right'\n"
                           "3 <-> 'up'\nEnter your step (enter digit): "))
        if action not in range(4):
            raise Exception
    except Exception:
        print("\nWRONG STEP!!! You should enter one if this digits: 0, 1, 2, 3.")
        continue

    new_state, reward,is_done, info = env.step(action)

env.render()
