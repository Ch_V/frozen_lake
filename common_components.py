from time import sleep

import gym
import numpy as np


GAMMA = 0.99  # step discounting coefficient
EPISODES_N = 10000  # number of episodes to evaluate a policy and policy_score
env = gym.make('FrozenLake-v1')  # 'FrozenLake-v1'    'FrozenLake8x8-v1'


n_states = len(env.env.P)
n_actions = len(env.env.P[0])
i2a = {0: 'left', 1: 'down', 2: 'right', 3: 'up'}  # integer-to-action


def get_random_policy() -> np.array:
    """Generates and returns random policy - numpy array of length n_states-1"""
    return np.random.randint(0, n_actions, (n_states,))  # We don't need any policy for (G) and for all (H) states really


def p2v(policy: np.array, n: int = EPISODES_N) -> np.array:
    """Evaluates policy through n episodes. Returns estimated V-function."""
    # V = np.random.rand(n_states)  # 'Capital V' global, final V-function that we will be iterated (starts with random)
    V = np.zeros(n_states)  # Capital 'V' - global, final V-function that we will be iterated (starts with zeros)
    for _ in range(n):
        v = np.zeros(n_states)  # small 'v' - local for episode v-function
        for state in env.env.P:
            action = policy[state].item()
            for prob, state_new, reward, is_done in env.env.P[state][action]:
                v[state] += prob * (reward + GAMMA * V[state_new])
        V = v
    return V


def v2best_policy(V: np.array) -> np.array:
    """Returns best policy based on input V-function values and known transition law."""
    P = np.zeros(n_states).astype('int8')  # policy (strategy)
    for state in range(n_states):
        action_v = np.zeros(n_actions)  # actions values for current state
        for action in range(n_actions):
            for prob, new_state, reward, is_done in env.env.P[state][action]:
                action_v[action] += prob * (reward + GAMMA * V[new_state])
        P[state] = action_v.argmax()
    return P


def q2best_policy(Q: np.array) -> np.array:
    """Returns best policy based on input Q-function values."""
    P = np.zeros(n_states).astype('int8')  # policy (strategy)
    for state in range(n_states):
        P[state] = Q[state].argmax()
    return P


def play_episode(policy: np.array, render: bool = False, slp: float = 0):
    """ Plays episode once with policy policy.
    render - should the play be rendered with slp sleep-time between steps.
    Returns total reward and list of states.
    """
    state = env.reset()
    total_reward = 0
    states = []
    while True:
        if render:
            env.render()
        sleep(slp)
        states.append(state)
        state, reward, is_done, info = env.step(policy[state].item())
        total_reward += reward
        if is_done:
            break
    return total_reward, states


def policy_score(policy, n=EPISODES_N):
    """ Estimates policy by running n episodes.
    Return mean policy score.
    """
    policy_score_ = 0
    for i in range(n):
        policy_score_ += play_episode(policy)[0]
    return policy_score_ / n


if __name__ == '__main__':
    pass
    play_episode(np.array([0, 3, 3, 3, 0, 0, 0, 0, 3, 1, 0, 0, 0, 2, 1, 0]), render=True, slp=1)
    # print(policy_score(np.array([0., 3., 3., 3.,
    #                              0., 0., 0., 0.,
    #                              3., 1., 0., 0.,
    #                              0., 2., 1., 0.]), n=EPISODES_N))

## FrozenLake-v1
# V -> [0.5420, 0.4988, 0.4707, 0.4569, 0.5585, 0.0000, 0.3583, 0.0000, 0.5918, 0.6431, 0.6152, 0.0000, 0.0000, 0.7417, 0.8628, 0.0000])
# P -> [0. 3. 3. 3. 0. 0. 0. 0. 3. 1. 0. 0. 0. 2. 1. 0.]
# score ~ 74%

## FrozenLake8x8-v1
# V ->  [0.4146, 0.4272, 0.4461, 0.4683, 0.4924, 0.5166, 0.5353, 0.541 ,
#        0.4117, 0.4212, 0.4375, 0.4584, 0.4832, 0.5135, 0.5458, 0.5574,
#        0.3968, 0.3938, 0.3755, 0.    , 0.4217, 0.4938, 0.5612, 0.5859,
#        0.3693, 0.353 , 0.3065, 0.2004, 0.3008, 0.    , 0.569 , 0.6283,
#        0.3327, 0.2914, 0.1973, 0.    , 0.2893, 0.362 , 0.5348, 0.6897,
#        0.3061, 0.    , 0.    , 0.0863, 0.2139, 0.2727, 0.    , 0.772 ,
#        0.2889, 0.    , 0.0577, 0.0475, 0.    , 0.2505, 0.    , 0.8778,
#        0.2804, 0.2008, 0.1273, 0.    , 0.2396, 0.4864, 0.7371, 0.    ]
# P -> [3. 2. 2. 2. 2. 2. 2. 2. 3. 3. 3. 3. 3. 2. 2. 1. 3. 3. 0. 0. 2. 3. 2. 1. 3. 3. 3. 1. 0. 0. 2. 2.
#       0. 3. 0. 0. 2. 1. 3. 2. 0. 0. 0. 1. 3. 0. 0. 2. 0. 0. 1. 0. 0. 0. 0. 2. 0. 1. 0. 0. 1. 2. 1. 0.]
# score ~ 86%